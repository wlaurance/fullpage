import * as React from 'react'
import * as ReactDOM from 'react-dom'
import './ui.css'

declare function require(path: string): any

interface IProps {}
interface IState {
  urls: string[],
  inputCount: number,
  errors: string,
  loadingMessage: string,
}

class App extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      urls: [''],
      inputCount: 1,
      errors: null,
      loadingMessage: '',
    };
  }
  getImageInfoForUrl(url) {
    return this.getImageForUrl(url)
    .then((bytes) => {
      return this.getDimensionsForUrl(url)
        .then((dimensions) => {
          return {
            url,
            bytes,
            dimensions,
          }
        });
    });
  }
  async create() {
    const {
      urls,
    } = this.state;
    if (urls.length === 0) {
      return this.setState({ errors: 'Please enter at least one URL' });
    }
    const filtered = urls.filter(e => e);
    this.setState({ loadingMessage: `Processing ${filtered.length} item(s)`});
    const screenshots = [];
    for (let i = 0; i < filtered.length; i++) {
      const screenshot = await this.getImageInfoForUrl(filtered[i])
      screenshots.push(screenshot);
    }
    parent.postMessage({ pluginMessage: { type: 'screenshots', screenshots } }, '*');
  }
  cancel() {
    parent.postMessage({ pluginMessage: { type: 'cancel' } }, '*');
  }
  handleChange(event, inputNum) {
    const {
      target: {
        value,
      },
    } = event;
    this.setState(({ urls }) => {
      const copy = [...urls];
      copy[inputNum] = value;
      return {
        urls: copy,
      };
    });
  }
  addInput() {
    this.setState(({ inputCount, urls }) => ({ inputCount: inputCount + 1, urls: [...urls, '']}));
  }
  removeInput(index) {
    this.setState(({ urls }) => {
      const copy = [...urls];
      copy.splice(index, 1);
      return {
        urls: copy,
        inputCount: copy.length,
      };
    });
  }
  getImageForUrl(url) {
    const p = new Promise((resolve, reject) => {
      var oReq = new XMLHttpRequest();
      const appUrl = `https://screenshotter.wlaurance.com?url=${url}`;
      oReq.open("GET", appUrl, true);
      oReq.responseType = "arraybuffer";
  
      var self = this;
      oReq.onload = function (oEvent) {
        self.setState({ loadingMessage: `Processed ${url}`});
        var eight = new Uint8Array(oReq.response); // Note: not oReq.responseText
        if (eight) {
          resolve(eight);
        }
      };
  
      oReq.send(null);
    });
    return p;
  }
  getDimensionsForUrl(url) {
    return new Promise((resolve, reject) => {
      var oReq = new XMLHttpRequest();
      const appUrl = `https://screenshotter.wlaurance.com?size=true&url=${url}`;
      oReq.open("GET", appUrl, true);

      oReq.onload = function (oEvent) {
        try {
          const dimensions = JSON.parse(oReq.responseText);
          resolve(dimensions);
        } catch (e) {
          reject(e);
        }
      };

      oReq.send(null);
    });
  }
  handleKeyPress(e, i) {
    if (e.keyCode === 13 && i === this.state.inputCount - 1) {
      this.addInput();
    }
  }
  render() {
    const {
      inputCount,
    } = this.state;
    return <div>
      <h2>FullPage</h2>
      {this.state.loadingMessage && (
        <p>{this.state.loadingMessage}</p>
      )}
      {this.state.errors && (
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <p style={{ color: 'red' }}>{this.state.errors}</p>
          <p onClick={() => this.setState({ errors: null })}>x</p>
        </div>
      )}
      {[...Array(inputCount).keys()].map((i) => (
        <div key={i} style={{ display: 'flex', justifyContent: 'space-around' }}>
          <input placeholder="Url" onKeyDown={(e) => this.handleKeyPress(e, i)} value={this.state.urls[i]} onChange={(e) => this.handleChange(e, i)}></input>
          {this.state.urls[i] && <button onClick={() => this.removeInput(i) }>Remove</button>}
        </div>
      ))} 
      <button style={{ marginTop: '10px' }} onClick={() => this.addInput()}>Add</button>
      <div style={{ marginTop: '10px', display: 'flex', justifyContent: 'space-around' }}>
        <button onClick={() => this.create()}>Create</button>
        <button onClick={() => this.cancel()}>Cancel</button>
      </div>
    </div>
  }
}

ReactDOM.render(<App />, document.getElementById('react-page'))