// This plugin will open a modal to prompt the user to enter a number, and
// it will then create that many rectangles on the screen.
// This file holds the main code for the plugins. It has access to the *document*.
// You can access browser APIs in the <script> tag inside "ui.html" which has a
// full browser enviroment (see documentation).
// This shows the HTML page in "ui.html".
figma.showUI(__html__);
// Calls to "parent.postMessage" from within the HTML page will trigger this
// callback. The callback will be passed the "pluginMessage" property of the
// posted message.
figma.ui.onmessage = msg => {
    // One way of distinguishing between different types of messages sent from
    // your HTML page is to use an object with a "type" property like this.
    if (msg.type === 'screenshots') {
        const s = msg.screenshots || [];
        const nodes = [];
        let x = 0;
        s.forEach(({ bytes, url, dimensions }) => {
            const rect = figma.createRectangle();
            const image = figma.createImage(bytes);
            const fills = clone(rect.fills);
            fills[0] = {
                type: "IMAGE",
                imageHash: image.hash,
                scaleMode: "FILL"
            };
            rect.name = url;
            rect.fills = fills;
            // Resize the rectangle to the size of the image
            const { height, width, } = dimensions;
            rect.resize(width, height);
            // Set the x value so the images line up next to eachother
            rect.x = x;
            // Set the x position to be at the boundary of image
            x = x + width;
            nodes.push(rect);
            figma.currentPage.appendChild(rect);
        });
        figma.currentPage.selection = [...nodes];
    }
    // Make sure to close the plugin when you're done. Otherwise the plugin will
    // keep running, which shows the cancel button at the bottom of the screen.
    figma.closePlugin();
};
function clone(val) {
    const type = typeof val;
    if (val === null) {
        return null;
    }
    else if (type === 'undefined' || type === 'number' ||
        type === 'string' || type === 'boolean') {
        return val;
    }
    else if (type === 'object') {
        if (val instanceof Array) {
            return val.map(x => clone(x));
        }
        else if (val instanceof Uint8Array) {
            return new Uint8Array(val);
        }
        else {
            let o = {};
            for (const key in val) {
                o[key] = clone(val[key]);
            }
            return o;
        }
    }
    throw 'unknown';
}
