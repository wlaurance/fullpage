import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './ui.css';
class App extends React.Component {
    render() {
        return React.createElement("div", null,
            React.createElement("h2", null, "Webpage Screenshot Generator"));
    }
}
const getImageForUrl = (url) => {
    const p = new Promise((resolve, reject) => {
        var oReq = new XMLHttpRequest();
        const appUrl = `https://screenshotter.wlaurance.com?size=true&url=${url}`;
        const corsUrl = `https://cors-anywhere.herokuapp.com/${appUrl}`;
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";
        oReq.onload = function (oEvent) {
            var eight = new Uint8Array(oReq.response); // Note: not oReq.responseText
            if (eight) {
                resolve(eight);
            }
        };
        oReq.send(null);
    });
    return p;
};
const getDimensionsForUrl = (url) => {
    return new Promise((resolve, reject) => {
        var oReq = new XMLHttpRequest();
        const appUrl = `https://screenshotter.wlaurance.com?size=true&url=${url}`;
        const corsUrl = `https://cors-anywhere.herokuapp.com/${appUrl}`;
        oReq.open("GET", corsUrl, true);
        oReq.onload = function (oEvent) {
            try {
                const dimensions = JSON.parse(oReq.responseText);
                resolve(dimensions);
            }
            catch (e) {
                reject(e);
            }
        };
        oReq.send(null);
    });
};
const getImageInfoForUrl = (url) => {
    return getImageForUrl(url)
        .then((bytes) => {
        return getDimensionsForUrl(url)
            .then((dimensions) => {
            return {
                url,
                bytes,
                dimensions,
            };
        });
    });
};
document.getElementById('create').onclick = () => {
    const urls = document.getElementById('urls').value.split('\n');
    Promise.all(urls.map(getImageInfoForUrl))
        .then(screenshots => {
        parent.postMessage({ pluginMessage: { type: 'screenshots', screenshots } }, '*');
    });
};
document.getElementById('cancel').onclick = () => {
    parent.postMessage({ pluginMessage: { type: 'cancel' } }, '*');
};
ReactDOM.render(React.createElement(App, null), document.getElementById('react-page'));
